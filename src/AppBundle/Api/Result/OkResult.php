<?php

namespace AppBundle\Api\Result;

use Symfony\Component\HttpFoundation\Response;

class OkResult extends AbstractResult
{
    /**
     * @inheritDoc
     */
    public function __construct($data, $message = null)
    {
        parent::__construct($data, Response::HTTP_OK, $message);
    }
}
