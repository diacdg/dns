<?php

namespace AppBundle\Api\Result;

use JMS\Serializer\Annotation\Groups;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractResult
{
    /**
     * @Groups("result")
     *
     * @var bool
     */
    protected $error;

    /**
     * @Groups("result")
     *
     * @var int
     */
    protected $code;

    /**
     * @Groups("result")
     *
     * @var string
     */
    protected $message;

    /**
     * @Groups("result")
     *
     * @var mixed
     */
    protected $data;

    /**
     * @param int $code
     * @param string $message
     * @param bool $error
     * @param mixed $data
     */
    public function __construct($data = null, $code = null, $message = null, $error = false)
    {
        $this->code = $code;

        if ($message === null && isset(Response::$statusTexts[$code])) {
            $message = Response::$statusTexts[$code];
        }

        $this->message = $message;
        $this->error = $error;
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function isError()
    {
        return $this->error;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}

