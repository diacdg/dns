<?php

namespace AppBundle\Api\Result;

use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;

class BadRequestResult extends AbstractResult
{
    /**
     * @inheritDoc
     */
    public function __construct($data, $message = null)
    {
        if ($data instanceof Form) {
            $errors = $data->getErrors();
            $data = $errors;
        }

        parent::__construct($data, Response::HTTP_BAD_REQUEST, $message);
    }
}
