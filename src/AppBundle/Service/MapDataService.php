<?php

namespace AppBundle\Service;

use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Metadata\PropertyMetadata;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use JMS\Serializer\Serializer;
use Metadata\MetadataFactory;

class MapDataService
{
    /** @var Serializer */
    protected $jmsSerializer;

    /** @var  MetadataFactory */
    protected $jmsSerializerMetadata;

    /**
     * MapDataService constructor.
     * @param Serializer $serializer
     * @param MetadataFactory $metadataFactory
     */
    public function __construct(Serializer $serializer, MetadataFactory $metadataFactory)
    {
        $this->jmsSerializer = $serializer;
        $this->jmsSerializerMetadata = $metadataFactory;
    }

    /**
     * later moving this logic to a specialized service
     *
     * @param array $data
     * @param object $targetEntity
     * @param array $serializationGroups
     */
    public function mapDataOnEntity($data, $targetEntity, $serializationGroups = [])
    {
        /** @var object $source */
        $sourceEntity = $this->jmsSerializer
            ->deserialize(
                json_encode($data),
                get_class($targetEntity),
                'json',
                DeserializationContext::create()->setGroups($serializationGroups)
            );
        $this->fillProperties($data, $targetEntity, $sourceEntity);
    }

    /**
     * later moving this logic to a specialized service
     *
     * @param array $params
     * @param object $targetEntity
     * @param object $sourceEntity
     */
    protected function fillProperties($params, $targetEntity, $sourceEntity)
    {
        $propertyAccessor = new PropertyAccessor();
        /** @var PropertyMetadata[] $propertyMetadata */
        $propertyMetadata = $this->jmsSerializerMetadata
            ->getMetadataForClass(get_class($sourceEntity))
            ->propertyMetadata;
        foreach ($propertyMetadata as $realPropertyName => $data) {
            $serializedPropertyName = $data->serializedName ?: $this->fromCamelCase($realPropertyName);
            if (array_key_exists($serializedPropertyName, $params)) {
                $newValue = $propertyAccessor->getValue($sourceEntity, $realPropertyName);
                $propertyAccessor->setValue($targetEntity, $realPropertyName, $newValue);
            }
        }
    }

    /**
     * later moving this logic to a specialized service
     *
     * @param string $input
     * @return string
     */
    protected function fromCamelCase($input)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }

        return implode('_', $ret);
    }
}
