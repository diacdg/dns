<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ExceptionController extends FOSRestController
{
    /**
     * This method is use to format exceptions messages
     *
     * @param Request $request
     * @param $exception
     * @return Response
     */
    public function showAction(Request $request, $exception)
    {
        //later we must log this request for debug
        $code = Response::HTTP_INTERNAL_SERVER_ERROR;
        $message = Response::$statusTexts[$code];

        if ($exception instanceof HttpException) {
            $code = $exception->getStatusCode();
            $message = $exception->getMessage();
        }

        $view = $this->view([
            'error' => true,
            'code'  => $code,
            'message' => $message,
            'data' => []
        ]);

        return $this->handleView($view);
    }
}
