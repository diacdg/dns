<?php

namespace AppBundle\Controller;

use AppBundle\Api\Result\BadRequestResult;
use AppBundle\Api\Result\CreatedResult;
use AppBundle\Api\Result\OkResult;
use AppBundle\Entity\Dns;
use AppBundle\Entity\DnsRecord;
use AppBundle\Form\Type\DnsRecordType;
use AppBundle\Form\Type\DnsType;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;

class ApiController extends FOSRestController
{
    /**
     * Get list of dns
     *
     * @ApiDoc(
     *      resource="/api/dns",
     *      description="Retrieve dns",
     *      statusCodes={
     *          Response::HTTP_NOT_FOUND="Dns not found",
     *          Response::HTTP_OK="Dns found"
     *      }
     * )
     *
     * @Get(
     *     path="dns",
     *     name="dns_get"
     * )
     *
     * @View(serializerGroups={"result"})
     *
     * @return \FOS\RestBundle\View\View
     */
    public function getDnsAction()
    {
        //later moving this logic to a specialized service
        $dns = $this->get('doctrine')->getManager()->getRepository('AppBundle:Dns')->findAll();

        return new OkResult($dns);
    }

    /**
     * Delete a dns
     *
     * @ApiDoc(
     *      resource="/api/dns/{id}",
     *      description="Delete a dns",
     *      requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "requirement"="\d+",
     *              "description"="Dns id"
     *          }
     *      },
     *      statusCodes={
     *          Response::HTTP_NOT_FOUND="Dns not found",
     *          Response::HTTP_NO_CONTENT="Dns was deleted"
     *      }
     * )
     *
     * @Delete(
     *     path="dns/{id}",
     *     name="dns_delete",
     *     requirements={"id"="[0-9]+"}
     * )
     *
     * @View(statusCode=Response::HTTP_NO_CONTENT)
     * @param Dns $dns
     * @return \FOS\RestBundle\View\View
     */
    public function deleteDnsAction(Dns $dns)
    {
        //later moving this logic to a specialized service
        $em = $this->get('doctrine')->getManager();
        foreach ($dns->getRecords() as $dnsRecord) {
            $em->remove($dnsRecord);
        }
        $em->remove($dns);
        $em->flush();

        return null;
    }

    /**
     * Create a dns
     *
     * @ApiDoc(
     *      resource="/api/dns",
     *      description="Create a dns",
     *      statusCodes={
     *          Response::HTTP_BAD_REQUEST ="Bad request or invalid data",
     *          Response::HTTP_CREATED="Dns created"
     *      }
     * )
     *
     * @View(statusCode=Response::HTTP_CREATED)
     *
     * @Post(
     *     path="dns",
     *     name="dns_post"
     * )
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function postDnsAction(Request $request)
    {
        $form = $this->createForm(DnsType::class);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            /** @var Dns $dns */
            $dns = $form->getData();

            //later moving this logic to a specialized service
            $em = $this->get('doctrine')->getManager();
            $em->persist($dns);
            $em->flush($dns);

            return $this->view(new CreatedResult(['id'=>$dns->getId()]))->setHeader('ETag', $dns->getId());
        }

        return $this->view(new BadRequestResult($form))->setStatusCode(Response::HTTP_BAD_REQUEST);
    }

    /**
     * Get list of dns records
     *
     * @ApiDoc(
     *     resource="/api/dns/{id}/records",
     *     description="Retrieve dns records",
     *     requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "requirement"="\d+",
     *              "description"="Dns id"
     *          }
     *      },
     *      statusCodes={
     *          Response::HTTP_NOT_FOUND="Dns not found",
     *          Response::HTTP_OK="Dns found"
     *      }
     * )
     *
     * @Get(
     *     path="dns/{id}/records",
     *     name="dns_records_get",
     *     requirements={"id"="[0-9]+"}
     * )
     *
     * @View(serializerGroups={"result"})
     * @param Dns $dns
     * @return \FOS\RestBundle\View\View
     */
    public function getDnsRecordsAction(Dns $dns)
    {
        return new OkResult($dns->getRecords());
    }

    /**
     * Create a dns
     *
     * @ApiDoc(
     *     resource="/api/dns/{id}/record",
     *     description="Create a dns",
     *     requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "requirement"="\d+",
     *              "description"="Dns id"
     *          }
     *      },
     *      statusCodes={
     *          Response::HTTP_BAD_REQUEST ="Bad request or invalid data",
     *          Response::HTTP_CREATED="Dns created"
     *      }
     * )
     *
     * @View(statusCode=Response::HTTP_CREATED)
     *
     * @Post(
     *     path="dns/{id}/record",
     *     name="dns_record_post",
     *     requirements={"id"="[0-9]+"}
     * )
     *
     * @param Dns $dns
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function postDnsRecordAction(Dns $dns, Request $request)
    {
        $form = $this->createForm(DnsRecordType::class);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            /** @var DnsRecord $dnsRecord */
            $dnsRecord = $form->getData();
            $dnsRecord->setDns($dns);

            //later moving this logic to a specialized service
            $em = $this->get('doctrine')->getManager();
            $em->persist($dnsRecord);
            $em->flush($dnsRecord);

            return $this->view(new CreatedResult(['id'=>$dnsRecord->getId()]))->setHeader('ETag', $dnsRecord->getId());
        }

        return $this->view(new BadRequestResult($form))->setStatusCode(Response::HTTP_BAD_REQUEST);
    }

    /**
     * Delete a dns record
     *
     * @ApiDoc(
     *      resource="/api/dns/record/{id}",
     *      description="Delete a dns record",
     *      requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "requirement"="\d+",
     *              "description"="Dns id"
     *          }
     *      },
     *      statusCodes={
     *          Response::HTTP_NOT_FOUND="Dns record not found",
     *          Response::HTTP_NO_CONTENT="Dns was deleted"
     *      }
     * )
     *
     * @Delete(
     *     path="dns/record/{id}",
     *     name="dns_record_delete",
     *     requirements={"id"="[0-9]+"}
     * )
     *
     * @View(statusCode=Response::HTTP_NO_CONTENT)
     * @param DnsRecord $dnsRecord
     * @return \FOS\RestBundle\View\View
     */
    public function deleteDnsRecordAction(DnsRecord $dnsRecord)
    {
        //later moving this logic to a specialized service
        $em = $this->get('doctrine')->getManager();
        $em->remove($dnsRecord);
        $em->flush($dnsRecord);

        return null;
    }

    /**
     * Update a dns record
     *
     * @ApiDoc(
     *      resource="/api/dns/record/{id}",
     *      description="Update a dns",
     *      requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "requirement"="\d+",
     *              "description"="Dns id"
     *          }
     *      },
     *      statusCodes={
     *          Response::HTTP_NOT_FOUND="Dns record not found",
     *          Response::HTTP_BAD_REQUEST ="Bad request or invalid data",
     *          Response::HTTP_NO_CONTENT="Dns was updated"
     *      }
     * )
     *
     * @Put(
     *     path="dns/record/{id}",
     *     name="dns_record_update",
     *     requirements={"id"="[0-9]+"}
     * )
     *
     * @View(statusCode=Response::HTTP_NO_CONTENT)
     * @param DnsRecord $dnsRecord
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function updateDnsRecordAction(DnsRecord $dnsRecord, Request $request)
    {
        $form = $this->createForm(DnsRecordType::class);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            /** @var DnsRecord $dnsRecord */
            $dnsRecordData = $request->request->all();
            $this->get('app.map_data')->mapDataOnEntity($dnsRecordData, $dnsRecord, ['result']);

            //later moving this logic to a specialized service
            $em = $this->get('doctrine')->getManager();
            $em->persist($dnsRecord);
            $em->flush($dnsRecord);

            return null;
        }

        return $this->view(new BadRequestResult($form))->setStatusCode(Response::HTTP_BAD_REQUEST);
    }
}
