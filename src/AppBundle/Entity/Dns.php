<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;

/**
 * Class Dns
 *
 * @ORM\Entity()
 * @ORM\Table(name="domain_name")
 */
class Dns
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Groups({"result"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", nullable=false, length=60)
     *
     * @Assert\Type(type="string")
     * @Assert\NotBlank()
     *
     * @Groups({"result"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="DnsRecord", mappedBy="dns")
     *
     * @Groups({"result"})
     */
    private $records;

    /**
     * Dns constructor.
     */
    public function __construct()
    {
        $this->records = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getRecords()
    {
        return $this->records;
    }

    /**
     * @param string $records
     * @return $this
     */
    public function setRecords($records)
    {
        $this->records = $records;

        return $this;
    }
}
