<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;

/**
 * Class Dns
 *
 * @ORM\Entity()
 * @ORM\Table(name="domain_name_record")
 */
class DnsRecord
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Groups({"result"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", nullable=false, length=60)
     *
     * @Assert\Type(type="string")
     * @Assert\NotBlank()
     *
     * @Groups({"result"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", nullable=false, length=15)
     *
     * @Assert\Choice(callback="getTypes")
     * @Assert\NotNull()
     *
     * @Groups({"result"})
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="value", nullable=false, length=100)
     *
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))$/")
     *
     * @Groups({"result"})
     */
    private $value;

    /**
     * @var Dns
     *
     * @ORM\ManyToOne(targetEntity="Dns", inversedBy="records")
     * @ORM\JoinColumn(name="dns_id", referencedColumnName="id")
     *
     * @Assert\Type(type="Dns")
     * @Assert\NotNull(groups={"extra"})
     * @Assert\Valid()
     */
    private $dns;

    /**
     * Dns constructor.
     */
    public function __construct()
    {
        $this->records = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Dns
     */
    public function getDns()
    {
        return $this->dns;
    }

    /**
     * @param Dns $dns
     * @return $this
     */
    public function setDns(Dns $dns)
    {
        $this->dns = $dns;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return ['A', 'AAAA', 'CNAME', 'MX', 'TXT', 'NS'];
    }
}
